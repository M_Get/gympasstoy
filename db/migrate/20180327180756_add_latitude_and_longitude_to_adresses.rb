class AddLatitudeAndLongitudeToAdresses < ActiveRecord::Migration[5.1]
  def change
    add_column :adresses, :latitude, :float
    add_column :adresses, :longitude, :float
    add_column :adresses, :address, :text
  end
end
