class CreateAdresses < ActiveRecord::Migration[5.1]
  def change
    create_table :adresses do |t|
      t.belongs_to :user, index: true
      t.string :kind
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end
end
