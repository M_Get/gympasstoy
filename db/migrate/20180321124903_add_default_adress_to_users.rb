class AddDefaultAdressToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :default_home_adress, :integer
    add_column :users, :default_work_adress, :integer
  end
end
