class AddAdressToGyms < ActiveRecord::Migration[5.1]
  def change
    add_column :adresses, :adressable_id, :integer, index: true
    add_column :adresses, :adressable_type, :string
  end
end
