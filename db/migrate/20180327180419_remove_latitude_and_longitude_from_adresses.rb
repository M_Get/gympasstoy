class RemoveLatitudeAndLongitudeFromAdresses < ActiveRecord::Migration[5.1]
  def change
    remove_column :adresses, :latitude
    remove_column :adresses, :longitude
  end
end
