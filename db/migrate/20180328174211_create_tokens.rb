class CreateTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :tokens do |t|
      t.belongs_to :user, index: true
      t.integer :gym_id
      t.timestamps
    end
  end
end
