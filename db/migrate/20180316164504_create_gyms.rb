class CreateGyms < ActiveRecord::Migration[5.1]
  def change
    create_table :gyms do |t|
      t.belongs_to :user, index: true
      t.string :name, null: false, unique: true
      t.time :open
      t.time :close

      t.timestamps
    end
  end
end
