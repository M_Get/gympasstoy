class AddNameToAdress < ActiveRecord::Migration[5.1]
  def change
    add_column :adresses, :name, :string
  end
end
