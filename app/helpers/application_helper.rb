module ApplicationHelper
  def google_map(location)
    "https://maps.googleapis.com/maps/api/staticmap?center=#{location}&size=300x300&key=#{ENV['GOOGLE_MAPS_KEY']}"
  end
end
