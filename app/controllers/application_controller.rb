class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  def require_manager
    unless current_user.is_manager?
      flash[:alert] = "You must be a gym manager for access to gyms"
      redirect_to root_path # halts request cycle
    end
  end
end
