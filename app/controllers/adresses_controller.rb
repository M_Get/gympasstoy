class AdressesController < ApplicationController
  def index
    @adress = current_user.adresses.new
  end
  def create
    @adress = current_user.adresses.new(adress_params)
    @adress.save
    redirect_to adresses_path
  end
  private

    def adress_params
      params.require(:adress).permit(:latitude, :longitude, :kind, :name, :address)
    end
end
