class GymsController < ApplicationController
  before_action :require_manager
  def index
    @gym = current_user.gyms.new
    @gym.build_adress
  end
  def create
    if current_user.is_manager?
      @gym = current_user.gyms.new(gym_params)
      @gym.save
      redirect_to gyms_path
    else
      redirect_to gyms_path
    end
  end
  private
    def gym_params
      params.require(:gym).permit(:name, :open, :close, adress_attributes: [:name, :address, :latitude, :longitude])
    end
end
