# User model
class User < ApplicationRecord
  include UserTypeAsker
  include UserAdressType
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  has_many :adresses, as: :adressable
  accepts_nested_attributes_for :adresses
  has_many :gyms
end
