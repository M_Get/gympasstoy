module UserAdressType
  def work_adresses
    adresses.where(kind: "work")
  end
  def home_adresses
    adresses.where(kind: "home")
  end
  def main_home_adress
    adresses.find(default_home_adress)
  end
  def main_work_adress
    adresses.find(default_work_adress)
  end
end
