module AdressTypeAsker
  def is_work_adress?
    kind == "work"
  end
  def is_home_adress?
    kind == "home"
  end
end
