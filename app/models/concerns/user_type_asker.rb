module UserTypeAsker
  def is_regular?
    user_type == "regular"
  end
  def is_manager?
    user_type == "manager"
  end
  def is_gympass?
    user_type == "gympass"
  end
end
