module GympassToyCommon
  def is_empty_integer?(data)
     data.blank? || data == 0
  end
end
