# Gym model
class Gym < ApplicationRecord
  belongs_to :user
  has_one :adress, as: :adressable
  accepts_nested_attributes_for :adress
end
