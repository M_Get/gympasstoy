# Adress model
class Adress < ApplicationRecord
  include AdressTypeAsker
  belongs_to :adressable, polymorphic: true, optional: true
  geocoded_by :address
  after_validation :geocode
end
