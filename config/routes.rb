Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  devise_scope :user do
    post 'update_main_home_adress', to: 'users/registrations#update_main_home_adress'
    post 'update_main_work_adress', to: 'users/registrations#update_main_work_adress'
  end
  resources :adresses
  resources :gyms
end
